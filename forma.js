$(document).ready(function () {

    $(".mymagicoverbox").click(function () {
        var page = 2;
        history.pushState({ page: 2 }, "title 2", "formIsOpen");
        MessageForm(page);
        return false;
    });

    function MessageForm(page) {
        if (page === 2) {
            $("#myfond_gris").fadeIn(300);
            $("#box_1").fadeIn(300);
            $("#myfond_gris").attr("opendiv", iddiv);
            return false;
        }
        else {
            var iddiv = $("#myfond_gris").attr("opendiv");
            $("#myfond_gris").fadeOut(300);
            $("#box_1").fadeOut(300);
            history.pushState({ page: 1 }, "title 1", "./");
            return false;
        }
    }

    $("#myfond_gris, .mymagicoverbox_fermer").click(function () {
        var page = 1;
        history.pushState({ page: 1 }, "title 1", "./");
        MessageForm(page);
        return false;
    });

    $(function () {
        $(".ajaxForm").submit(function (e) {
            e.preventDefault();
            var href = $(this).attr("action");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: href,
                data: $(this).serialize(),
                success: function (response) {
                    if (response.status == "success") {
                        alert("Подтвердите отправку");
                    } else {
                        alert("Если вы это видите значит что-то пошло не так " + response.message);
                    }
                }
            });
        });
    });
    window.addEventListener("popstate", e => {
        MessageForm(e.state.page);
    });

    history.replaceState({page: null}, "Default state", "./");
});
